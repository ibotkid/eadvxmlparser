#include "tinyxml/tinyxml.h"
#include "include_exmlparser_header.h"
#include <Windows.h>
#include <stdio.h>
#include "elib/krnllib.h"
#include "elib/fnshare.h"


/*// 返回数组的数据部分首地址及成员数目。
void* GetAryElementInfo(void* pAryData, LPINT pnElementCount)
{
	LPINT pnData = (LPINT)pAryData;
	INT nArys = *pnData++;  // 取得维数。
	// 计算成员数目。
	INT nElementCount = 1;
	while (nArys > 0)
	{
		nElementCount *= *pnData++;
		nArys--;
	}

	if (pnElementCount != NULL)
		*pnElementCount = nElementCount;
	return (void*)pnData;
}
*/

int GetAttributeCount(TiXmlElement* xe)
{
	TiXmlAttribute* attr = xe->FirstAttribute();
	int i = 0;
	if (attr) {
		for (TiXmlAttribute* curr = attr;
			curr != nullptr;
			curr = curr->Next())
			i++;
	}
	return i;
}


//XML文档
typedef struct _EXMLDocument {
	//真为已经创建 假为没有创建
	int CreateFlag;
	//文档
	TiXmlDocument* pXMLDoucument;
}_EXMLDocument, * EXMLDocument;

//加载
//str file,int encoding
ECMDIMPL(XmlDoc, 0, LoadXmlFile)(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
	EXMLDocument exml = (EXMLDocument)pArgInf[0].m_pCompoundData;
	LPSTR file = pArgInf[1].m_pText;
	int encoding = pArgInf[2].m_int;
	exml->pXMLDoucument = new TiXmlDocument();
	exml->CreateFlag = 1;
	if (encoding == 0)
		exml->pXMLDoucument->LoadFile(file, TIXML_ENCODING_UTF8);
	else if (encoding == 1)
		exml->pXMLDoucument->LoadFile(file, TIXML_ENCODING_UNKNOWN);
	//exml->pXMLRootNode = exml->pXMLDoucument->RootElement();
	pRetData->m_bool = TRUE;
}

//解析
//str xmltext
ECMDIMPL(XmlDoc, 1, ParserXml)(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
	EXMLDocument exml = (EXMLDocument)pArgInf[0].m_pCompoundData;
	LPSTR str = pArgInf[1].m_pText;
	exml->pXMLDoucument = new TiXmlDocument();
	exml->pXMLDoucument->Parse(str);
	//exml->pXMLRootNode = exml->pXMLDoucument->RootElement();
	exml->CreateFlag = 1;
	pRetData->m_bool = TRUE;
}

//创建
//str name
ECMDIMPL(XmlDoc, 2, CreateXml)(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
	EXMLDocument exml = (EXMLDocument)pArgInf[0].m_pCompoundData;
	LPSTR name = pArgInf[1].m_pText;
	if (exml->CreateFlag == 1)
	{
		delete	exml->pXMLDoucument;
	}
	TiXmlElement* root = new TiXmlElement(name);
	exml->pXMLDoucument = new TiXmlDocument();
	exml->pXMLDoucument->InsertEndChild(*root);
	pRetData->m_bool = TRUE;
}

//生成文本
ECMDIMPL(XmlDoc, 3, ToXmlString)(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
	EXMLDocument exml = (EXMLDocument)pArgInf[0].m_pCompoundData;
	pRetData->m_pText = (char*)exml->pXMLDoucument->Value();
}

//保存
//str file
ECMDIMPL(XmlDoc, 4, SaveXml)(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
	EXMLDocument exml = (EXMLDocument)pArgInf[0].m_pCompoundData;
	LPSTR file = pArgInf[1].m_pText;
	exml->pXMLDoucument->SaveFile(file);
	pRetData->m_bool = TRUE;
}

//取根节点
ECMDIMPL(XmlDoc, 5, GetRootNode)(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
	EXMLDocument exml = (EXMLDocument)pArgInf[0].m_pCompoundData;
	pRetData->m_int = (int)exml->pXMLDoucument->RootElement();
}

//取文档说明
ECMDIMPL(XmlDoc, 6, GetDeclaration)(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
	EXMLDocument exml = (EXMLDocument)pArgInf[0].m_pCompoundData;
	TiXmlDeclaration* declaration = exml->pXMLDoucument->ToDeclaration();
	if (declaration != nullptr) {
		LPSTR rst = new char[32];
		LPSTR v = (char*)declaration->Version();
		LPSTR e = (char*)declaration->Encoding();
		LPSTR s = (char*)declaration->Standalone();
		sprintf(rst, "%s,%s,%s",
			v == nullptr ? "" : v,
			e == nullptr ? "" : e,
			s == nullptr ? "" : s);
		pRetData->m_pText = rst;
	}
	else {
		pRetData->m_pText = (char*)"";
	}
}

//取子节点数
//int node
ECMDIMPL(XmlDoc, 7, GetChildCount)(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
	TiXmlElement* xe = (TiXmlElement*)pArgInf[1].m_int;
	if (xe == nullptr)
	{
		pRetData->m_int = 0;
		return;
	}
	TiXmlElement* attr = xe->FirstChildElement();
	int i = 0;
	if (attr) {
		for (TiXmlElement* curr = attr;
			curr != nullptr;
			curr = curr->NextSiblingElement())
			i++;
	}
	pRetData->m_int = i;
}

//取属性
//int node,str name
ECMDIMPL(XmlDoc, 8, GetAttribute)(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
	TiXmlElement* xe = (TiXmlElement*)pArgInf[1].m_int;
	LPSTR name = (LPSTR)pArgInf[2].m_pText;
	if (xe != nullptr)
	{
		pRetData->m_pText = (char*)xe->Attribute(name);
	}
	else {
		pRetData->m_pText = (char*)"";
	}
}

//取属性数
//int node
ECMDIMPL(XmlDoc, 9, GetAttributeCount)(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
	TiXmlElement* xe = (TiXmlElement*)pArgInf[1].m_int;
	if (xe != nullptr)
	{
		pRetData->m_int = GetAttributeCount(xe);
	}
	else {
		pRetData->m_int = 0;
	}
}

//取子节点
//int node,str name
ECMDIMPL(XmlDoc, 10, GetChildNode)(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
	TiXmlElement* xe = (TiXmlElement*)pArgInf[1].m_int;
	if (xe == nullptr)
	{
		pRetData->m_int = 0;
		return;
	}
	LPSTR name = (LPSTR)pArgInf[2].m_pText;
	TiXmlNode* tn = xe->FirstChild(name);
	if (tn != nullptr)
	{
		if (tn->Type() == TiXmlNode::TINYXML_ELEMENT)
		{
			pRetData->m_int = (int)tn;
		}
	}
	else
	{
		pRetData->m_int = 0;
	}
}

//添加节点
//int pnode,int cnode
ECMDIMPL(XmlDoc, 11, AddNode)(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
	TiXmlElement* xe = (TiXmlElement*)pArgInf[1].m_int;
	TiXmlElement* child = (TiXmlElement*)pArgInf[2].m_int;
	if (xe != nullptr && child != nullptr)
	{
		xe->InsertEndChild(*child);
		pRetData->m_bool = TRUE;
	}
	else {
		pRetData->m_bool = FALSE;
	}
}

//置属性
//int node,str name,str value
ECMDIMPL(XmlDoc, 12, SetAttribute)(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
	TiXmlElement* xe = (TiXmlElement*)pArgInf[1].m_int;
	if (xe == nullptr)
	{
		pRetData->m_bool = FALSE;
		return;
	}
	LPSTR name = (LPSTR)pArgInf[2].m_pText;
	LPSTR value = (LPSTR)pArgInf[3].m_pText;
	if (xe != nullptr)
	{
		xe->SetAttribute(name, value);
		pRetData->m_bool = TRUE;
	}
	else
	{
		pRetData->m_bool = FALSE;
	}
}

//插入节点
//int pnode,int cnode,int index
ECMDIMPL(XmlDoc, 13, InstNode)(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
	TiXmlElement* xe = (TiXmlElement*)pArgInf[1].m_int;
	TiXmlElement* child = (TiXmlElement*)pArgInf[1].m_int;
	if (xe == nullptr || child == nullptr)
	{
		pRetData->m_bool = FALSE;
		return;
	}
	int index = pArgInf[2].m_int;
	TiXmlElement* attr = xe->FirstChildElement();
	int i = 0;
	if (attr) {
		for (TiXmlElement* curr = attr;
			curr != nullptr;
			curr = curr->NextSiblingElement(), i++)
		{
			if (i == index)
			{
				xe->InsertAfterChild(curr, *child);
				pRetData->m_bool = TRUE;
				return;
			}
			i++;
		}
		if (i >= index)
		{
			xe->LinkEndChild(child);
			pRetData->m_bool = TRUE;
			return;
		}
	}
	xe->LinkEndChild(child);
	pRetData->m_bool = TRUE;
}

//取所有属性
//int node,str[] array
ECMDIMPL(XmlDoc, 14, GetAttributes)(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
	TiXmlElement* xe = (TiXmlElement*)pArgInf[1].m_int;
	if (xe == nullptr)
	{
		pRetData->m_int = 0;
		return;
	}
	NotifySys(NRS_FREE_ARY, (DWORD)pArgInf->m_dtDataType, (DWORD)pArgInf[2].m_ppAryData);
	TiXmlAttribute* attr = xe->FirstAttribute();
	int i = GetAttributeCount(xe);
	if (i == 0)
	{
		pRetData->m_int = 0;
		return;
	}
	i = i * 2;
	LPSTR* sarry = (LPSTR*)NotifySys(NRS_MALLOC, (DWORD)((i + 2) * sizeof(int)), 0);
	attr = xe->FirstAttribute();
	if (attr) {
		int j = 2;
		for (TiXmlAttribute* curr = attr;
			curr != nullptr;
			curr = curr->Next(), j += 2)
		{
			sarry[j] = (LPSTR)curr->Name();
			int k = j + 1;
			sarry[k] = (LPSTR)curr->Value();
		}
	}
	sarry[0] = (LPSTR)1;
	sarry[1] = (LPSTR)i;
	*pArgInf[2].m_ppAryData = sarry;  // 将新内容写回该数组变量。
	pRetData->m_int = i;
}

//取所有子节点
//int node,int[] array
ECMDIMPL(XmlDoc, 15, GetChildNodes)(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
	TiXmlElement* xe = (TiXmlElement*)pArgInf[1].m_int;
	if (xe == nullptr)
	{
		pRetData->m_int = 0;
		return;
	}
	TiXmlElement* attr = xe->FirstChildElement();
	int i = 0;
	if (attr) {
		for (TiXmlElement* curr = attr;
			curr != nullptr;
			curr = curr->NextSiblingElement())
			i++;
	}
	if (i == 0)
	{
		pRetData->m_int = 0;
		return;
	}

	NotifySys(NRS_FREE_ARY, (DWORD)pArgInf->m_dtDataType, (DWORD)pArgInf[2].m_ppAryData);

	int* array = (int*)NotifySys(NRS_MALLOC, (DWORD)((i + 2) * sizeof(int)), 0);
	attr = xe->FirstChildElement();
	if (attr) {
		int j = 2;
		for (TiXmlElement* curr = attr;
			curr != nullptr;
			curr = curr->NextSiblingElement(), j++)
		{
			array[j] = (int)curr;
		}
	}
	array[0] = 1;
	array[1] = i;
	*pArgInf[2].m_ppAryData = array;
	pRetData->m_int = i;
}

//移除子节点
//int node,int cnode
ECMDIMPL(XmlDoc, 16, RemoveChild)(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
	TiXmlElement* xe = (TiXmlElement*)pArgInf[1].m_int;
	TiXmlElement* child = (TiXmlElement*)pArgInf[2].m_int;
	if (xe != nullptr && child != nullptr)
	{
		xe->RemoveChild(child);
		pRetData->m_bool = TRUE;
	}
	else {
		pRetData->m_bool = FALSE;
	}
}

//移除属性
//int node,str name
ECMDIMPL(XmlDoc, 17, RemoveAttribute)(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
	TiXmlElement* xe = (TiXmlElement*)pArgInf[1].m_int;
	if (xe == nullptr)
	{
		pRetData->m_bool = FALSE;
		return;
	}
	LPSTR name = (LPSTR)pArgInf[2].m_pText;
	if (xe != nullptr)
	{
		xe->RemoveAttribute(name);
		pRetData->m_bool = TRUE;
	}
	else {
		pRetData->m_bool = FALSE;
	}
}

//释放文档
ECMDIMPL(XmlDoc, 18, ReleaseXml)(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
	EXMLDocument exml = (EXMLDocument)pArgInf[0].m_pCompoundData;
	exml->CreateFlag = 0;
	delete exml->pXMLDoucument;
	pRetData->m_bool = TRUE;
}

//创建节点
//str name
ECMDIMPL(XmlDoc, 19, CreateNode)(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
	LPSTR name = (LPSTR)pArgInf[1].m_pText;
	TiXmlElement* xe = new TiXmlElement(name);
	pRetData->m_int = (int)xe;
}

//释放节点
//int node
ECMDIMPL(XmlDoc, 20, ReleaseNode)(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
	TiXmlElement* xe = (TiXmlElement*)pArgInf[1].m_int;
	if (xe != nullptr)
	{
		delete xe;
		pRetData->m_bool = TRUE;
	}
	else
	{
		pRetData->m_bool = FALSE;
	}
}

//添加注释
//int node,str value
ECMDIMPL(XmlDoc, 21, AddComment)(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
	TiXmlElement* xe = (TiXmlElement*)pArgInf[1].m_int;
	LPSTR value = (LPSTR)pArgInf[2].m_pText;
	if (xe != nullptr)
	{
		TiXmlComment* cmt = new TiXmlComment(value);
		xe->InsertEndChild(*cmt);
		pRetData->m_bool = TRUE;
	}
	else
	{
		pRetData->m_bool = FALSE;
	}
}

//取节点值
//int node
ECMDIMPL(XmlDoc, 22, GetNodeValue)(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
	TiXmlElement* xe = (TiXmlElement*)pArgInf[1].m_int;
	if (xe != nullptr)
	{
		pRetData->m_pText = (char*)xe->GetText();
	}
	else {
		pRetData->m_pText = (char*)"";
	}
}

//取节点名称
//int node
ECMDIMPL(XmlDoc, 23, GetNodeName)(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
	TiXmlElement* xe = (TiXmlElement*)pArgInf[1].m_int;
	if (xe != nullptr)
	{
		pRetData->m_pText = (char*)xe->Value();
	}
	else {
		pRetData->m_pText = (char*)"";
	}
}

//置节点值
//int node,str val
ECMDIMPL(XmlDoc, 24, SetNodeValue)(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
	TiXmlElement* xe = (TiXmlElement*)pArgInf[1].m_int;
	LPSTR value = pArgInf[2].m_pText;
	if (xe != nullptr && value != nullptr)
	{
		TiXmlNode* node = xe->FirstChild();
		if (node != nullptr)
		{
			if (node->Type() == node->TINYXML_TEXT)
			{
				xe->ReplaceChild(node, TiXmlText(value));
				pRetData->m_bool = TRUE;
				return;
			}
		}
		else
		{
			xe->InsertEndChild(TiXmlText(value));
			pRetData->m_bool = TRUE;
			return;
		}
	}
	pRetData->m_bool = FALSE;
}

//置节点名
//int node,str name
ECMDIMPL(XmlDoc, 25, SetNodeName)(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
	TiXmlElement* xe = (TiXmlElement*)pArgInf[1].m_int;
	LPSTR name = pArgInf[2].m_pText;
	if (xe != nullptr && name != nullptr)
	{
		xe->SetValue(name);
		pRetData->m_bool = TRUE;
	}
	else {
		pRetData->m_bool = FALSE;
	}
}


//包含有属性
//int node
ECMDIMPL(XmlDoc, 26, ContainsAttr)(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
	TiXmlElement* xe = (TiXmlElement*)pArgInf[1].m_int;
	if (xe != nullptr)
	{
		if (xe->FirstAttribute() != nullptr)
			pRetData->m_bool = TRUE;
	}
	else {
		pRetData->m_bool = FALSE;
	}
}


//包含有子级
//int node
ECMDIMPL(XmlDoc, 27, ContainsChild)(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
	TiXmlElement* xe = (TiXmlElement*)pArgInf[1].m_int;
	if (xe != nullptr)
	{
		if (xe->FirstChildElement() != nullptr)
			pRetData->m_bool = TRUE;
	}
	else {
		pRetData->m_bool = FALSE;
	}
}

//存在属性
//int node,str name
ECMDIMPL(XmlDoc, 28, ExistsAttr)(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
	TiXmlElement* xe = (TiXmlElement*)pArgInf[1].m_int;
	LPSTR name = pArgInf[2].m_pText;
	if (xe != nullptr && name != nullptr)
	{
		if (xe->Attribute(name) != nullptr)
			pRetData->m_bool = TRUE;
	}
	else {
		pRetData->m_bool = FALSE;
	}
}

//存在子节点
//int node,str name
ECMDIMPL(XmlDoc, 29, ExistsChild)(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
	TiXmlElement* xe = (TiXmlElement*)pArgInf[1].m_int;
	LPSTR name = pArgInf[2].m_pText;
	if (xe != nullptr && name != nullptr)
	{
		if (xe->FirstChildElement(name) != nullptr)
			pRetData->m_bool = TRUE;
	}
	else {
		pRetData->m_bool = FALSE;
	}
}
