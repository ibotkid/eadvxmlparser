#if !defined(__E_STATIC_LIB)
#include "include_exmlparser_header.h"

//TODO 静态库需要的部分,会记录所有的函数名到数组里,静态编译的时候需要取回命令名
static ARG_INFO g_argumentInfo_exmlparser_global_var[] = 
{
    // 1=参数名称, 2=参数详细解释, 3=指定图像索引,从1开始,0表示无, 4=图像数目(用作动画)
    // 5=参数类型, 6=参数默认值,   7=参数标志 AS_ 开头常量
    // AS_HAS_DEFAULT_VALUE         有默认值,倒数第二个参数是默认值
    // AS_DEFAULT_VALUE_IS_EMPTY    默认值为空,有可空标志
    // AS_RECEIVE_VAR               只能传递变量,相当于传引用,传递过来的肯定不是数组
    // AS_RECEIVE_VAR_ARRAY         传递过来的肯定是数组变量引用
    // AS_RECEIVE_VAR_OR_ARRAY      传递变量或者数组变量引用
    // AS_RECEIVE_ARRAY_DATA        传递数组
    // AS_RECEIVE_ALL_TYPE_DATA     传递数组/非数组
    // AS_RECEIVE_VAR_OR_OTHER      可以传递 变量/数据/返回值数据

    /*000*/ {"文件路径", "欲加载的XML文件路径", 0, 0, SDT_TEXT, 0, NULL},
    /*001*/ {"文件编码", "文件编码方式，0为UTF-8 1为ANSI，默认为0", 0, 0, SDT_INT, 0, AS_HAS_DEFAULT_VALUE},
    
    /*002*/ {"文本", "XML文本内容", 0, 0, SDT_TEXT, 0, NULL},
    
    /*003*/ {"文件路径", "XML保存文件路径", 0, 0, SDT_TEXT, 0, NULL},

    /*004*/ {"节点句柄", "要操作的XML节点句柄", 0, 0, SDT_INT, 0,NULL},

    /*005*/ {"句柄", "要操作的XML节点句柄", 0, 0, SDT_INT, 0,NULL},
    /*006*/ {"名称", "属性名称", 0, 0, SDT_TEXT, 0,NULL},

    /*007*/ {"节点句柄", "要操作的XML节点句柄", 0, 0, SDT_INT, 0,NULL},
    /*008*/ {"名称", "子节点名称", 0, 0, SDT_TEXT, 0,NULL},

    /*009*/ {"节点句柄", "要操作的XML节点句柄", 0, 0, SDT_INT, 0,NULL},
    /*010*/ {"子节点", "要操作的XML子节点句柄", 0, 0, SDT_INT, 0,NULL},

    /*011*/ {"节点句柄", "要操作的XML节点句柄", 0, 0, SDT_INT, 0,NULL},
    /*012*/ {"名称", "属性名称", 0, 0, SDT_TEXT, 0,NULL},
    /*013*/ {"值", "属性内容", 0, 0, SDT_TEXT, 0,NULL},

    /*014*/ {"节点句柄", "要操作的XML节点句柄", 0, 0, SDT_INT, 0,NULL},
    /*015*/ {"子节点", "要操作的XML子节点句柄", 0, 0, SDT_INT, 0,NULL},
    /*016*/ {"位置", "插入位置", 0, 0, SDT_INT, 0,NULL},

    /*017*/ {"节点句柄", "要操作的XML节点句柄", 0, 0, SDT_INT, 0,NULL},
    /*018*/ {"数组", "存放属性名称与值的数组，为文本型", 0, 0, SDT_TEXT, 0,AS_RECEIVE_VAR_ARRAY},
    
    /*019*/ {"节点句柄", "要操作的XML节点句柄", 0, 0, SDT_INT, 0,NULL},
    /*020*/ {"数组", "存放子节点句柄的数组，为整数型", 0, 0, SDT_INT, 0,AS_RECEIVE_VAR_ARRAY},

    /*021*/ {"节点句柄", "要操作的XML节点句柄", 0, 0, SDT_INT, 0,NULL},
    /*022*/ {"子节点", "要操作的XML子节点句柄", 0, 0, SDT_INT, 0,NULL},

    /*023*/ {"节点句柄", "要操作的XML节点句柄", 0, 0, SDT_INT, 0,NULL},
    /*024*/ {"属性名称", "要移除的属性名称", 0, 0, SDT_TEXT, 0,NULL},

    /*025*/ {"名称", "新节点名称", 0, 0, SDT_TEXT, 0,NULL},

    /*026*/ {"节点句柄", "要操作的XML节点句柄", 0, 0, SDT_INT, 0,NULL},

    /*027*/ {"节点句柄", "要操作的XML节点句柄", 0, 0, SDT_INT, 0,NULL},
    /*028*/ {"内容", "注释内容", 0, 0, SDT_TEXT, 0,NULL},

    /*029*/ {"名称", "根节点名称", 0, 0, SDT_TEXT, 0,NULL},

    /*030*/ {"节点句柄", "要操作的XML节点句柄", 0, 0, SDT_INT, 0,NULL},
    /*031*/ {"节点值", "需要设置的节点值", 0, 0, SDT_TEXT, 0,NULL},

    /*032*/ {"节点句柄", "要操作的XML节点句柄", 0, 0, SDT_INT, 0,NULL},
    /*033*/ {"节点新名称", "需要设置的新名称值", 0, 0, SDT_TEXT, 0,NULL},

    /*034*/ {"节点句柄", "要操作的XML节点句柄", 0, 0, SDT_INT, 0,NULL},
    /*035*/ {"属性名称", "需要判断的属性名称", 0, 0, SDT_TEXT, 0,NULL},

    /*036*/ {"节点句柄", "要操作的XML节点句柄", 0, 0, SDT_INT, 0,NULL},
    /*037*/ {"子级节点名称", "需要判断子级节点名称", 0, 0, SDT_TEXT, 0,NULL},
};

#ifdef _DEBUG    // 这里是为了确认参数序号是否正确, 成员数比最后一个序号大1就是正确
const int dbg_cmd_arg_count__ = sizeof(g_argumentInfo_exmlparser_global_var) / sizeof(g_argumentInfo_exmlparser_global_var[0]);
#endif


#define EXMLPARSER_DEF_CMDINFO(_index, _szName, _szEgName, _szExplain, _shtCategory, _wState, _dtRetValType, _wReserved, _shtUserLevel, _shtBitmapIndex, _shtBitmapCount, _nArgCount, _pBeginArgInfo) \
    { _szName, ______E_FNENAME(_szEgName), _szExplain, _shtCategory, _wState, _dtRetValType, _wReserved, _shtUserLevel, _shtBitmapIndex, _shtBitmapCount, _nArgCount, _pBeginArgInfo },


// 易语言每个命令的定义, 会把信息显示在支持库列表里, 这里每个成员都是详细的描述一个命令的信息
CMD_INFO g_cmdInfo_exmlparser_global_var[] = 
{
    EXMLPARSER_DEF(EXMLPARSER_DEF_CMDINFO)
};

int g_cmdInfo_exmlparser_global_var_count = sizeof(g_cmdInfo_exmlparser_global_var) / sizeof(g_cmdInfo_exmlparser_global_var[0]);

#endif

