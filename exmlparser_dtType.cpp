#ifndef __E_STATIC_LIB
#include "include_exmlparser_header.h"

// XML文档 下的方法索引
static INT s_dtCmdIndexexmlparser_EXMLDocument_static_var_00[] =
{
	0,1,2,3,4,5,6,
	7,8,9,10,11,12,
	13,14,15,16,17,
	18,19,21,22,23,
	24,25,26,27,28,
	29,
};
static int s_dtCmdIndexexmlparser_EXMLDocument_static_var_count_00 =
sizeof(s_dtCmdIndexexmlparser_EXMLDocument_static_var_00) / sizeof(s_dtCmdIndexexmlparser_EXMLDocument_static_var_00[0]);

// XML文档
static LIB_DATA_TYPE_ELEMENT s_objEventexmlparser_EXMLDocument_static_var_00[] =
{
	/*000*/ {SDT_INT, 0, "是否创建", "CreateFlag", "1为已经创建 0为没有创建", NULL, (INT)FALSE},
	/*001*/ {SDT_INT, 0, "文档指针", "pXMLDoucument", NULL, LES_HIDED, (INT)0x00000000},
};


static int s_objEventexmlparser_EXMLDocument_static_var_count_00 =
sizeof(s_objEventexmlparser_EXMLDocument_static_var_00) / sizeof(s_objEventexmlparser_EXMLDocument_static_var_00[0]);

LIB_DATA_TYPE_INFO g_DataType_exmlparser_global_var[] =
{
	//1=中文名字,2=英文名字,3=详细解释,4=命令数量,5=索引值,6=标志 LDT_
	//类型为窗口或菜单组件时有效 7=资源ID,8=事件数量,9=组件事件数组,10=属性数  11=属性数组 12=组件交互子程序
	//不为窗口、菜单组件或为枚举数据类型时才有效 13=成员数量,14=成员数据数组
			{ "XML文档", "EXMLDocument", "该数据类型提供对W3C标准XML文件的读写支持",
				s_dtCmdIndexexmlparser_EXMLDocument_static_var_count_00, s_dtCmdIndexexmlparser_EXMLDocument_static_var_00, _DT_OS(__OS_WIN),
				0, NULL, NULL,
				NULL, NULL,
				NULL,
				s_objEventexmlparser_EXMLDocument_static_var_count_00, s_objEventexmlparser_EXMLDocument_static_var_00
			},
};
int g_DataType_exmlparser_global_var_count =
sizeof(g_DataType_exmlparser_global_var) / sizeof(g_DataType_exmlparser_global_var[0]);

#endif

